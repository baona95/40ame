<?php

if (php_sapi_name() !== 'cli')
{
    die;
}

$input = $argv[1];
$output = $argv[2];
$startLine = (int) $argv[3];
$endLine = (int) $argv[4];

$content = file_get_contents($input);

$lineNo = 0;
$lines = preg_split('#\n|\r\n#', $content);

$replacedLines = [];

$patterns = array_keys(replacements());
$replacements = array_values(replacements());

foreach ($lines as $line)
{
    $lineNo++;
    if ($lineNo < $startLine)
    {
        continue;
    }

    $line = preg_replace($patterns, $replacements, trim($line));

    $replacedLines[] = $line;

    if ($lineNo == $endLine)
    {
        break;
    }
}

file_put_contents($output, finalize(implode("\n", $replacedLines)));

function finalize(string $string): string
{
    return str_replace('^@^', '', $string);
}

function replacements(): array
{
    return [
        '#^setwindow.*#' => "'...',",

        '#^bgm\s*\"m\\\\(.*)\.wav\".*#' => "'play sound $1 loop',",
        '#^bgmonce\s*\"m\\\\(.*)\.wav\".*#' => "'play sound $1',",
        '#^wave\s*\"m\\\\(.*)\.wav\".*#' => "'play sound $1',",
        '#^bgm\s*\"m\\\\(.*)\.(mp3|mid)\".*#' => "'play music $1 loop',",
        '#^bgmonce\s*\"m\\\\(.*)\.(mp3|mid)\".*#' => "'play music $1',",
        '#^bgmstop$#' => "'stop music', 'stop sound',",
        '#^mp3stop$#' => "'stop music',",
        '#^bgmfadeout.*#' => "'...',",
        '#^dwave\s*.*?,\"m\\\\(.*)\.wav\".*#' => "'play sound $1',",

        '#^bg\s*\"e\\\\(.*)\.jpg\".*#' => "'scene $1',",
        '#^bg\s*\#([a-z0-9]+),.*#i' => "'scene #$1',",

        '#^br#' => "'...',",
        '#^erasetextwindow.*#' => "'clear',",
        
        '#^\^(.*)\\\\$#' => "'$1\\n',",
        '#^\^(.*)\^@\^$#' => "'$1',",
        '#^(.*)\^@\^$#' => "'$1',",
        '#^\^(.*)$#' => "'$1',",

        '#^!w(\d+)$#' => "'wait $1',",
        '#^!d(\d+)$#' => "'wait $1',",
    ];
}